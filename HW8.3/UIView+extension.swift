
import Foundation
import UIKit

extension UIView {
    func dropShadow(_ radius: CGFloat = 15) {
            layer.masksToBounds = false
            layer.shadowColor = UIColor.gray.cgColor
            layer.shadowOpacity = 0.8
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowRadius = 10
            layer.cornerRadius = radius
            layer.shadowPath = UIBezierPath(rect: bounds).cgPath
            layer.shouldRasterize = true
        
        }

}
extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
