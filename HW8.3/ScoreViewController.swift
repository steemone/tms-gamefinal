

import UIKit

class ScoreViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var backButtonOutlet: UIButton!
    
    var scoresArray: [Save] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let array = UserDefaults.standard.value([Save].self, forKey: "scores") else {return}
        self.scoresArray = array
        self.backButtonOutlet.dropShadow(15)
        self.backButtonOutlet.titleLabel?.font = UIFont(name: "Crosterian", size: 7.5)
        self.scoreLabel.dropShadow(15)
        self.scoreLabel.font = UIFont(name: "Crosterian", size: 35)
    }
    
    @IBAction func pressButtonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ScoreViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoresArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        let sortedScoresArray = scoresArray.sorted(by: {$0.scorePoint! > $1.scorePoint!} )
        cell.configure(object: sortedScoresArray[indexPath.row])
        return cell
    }
    
}
