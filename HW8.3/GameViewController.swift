import UIKit
import CoreMotion

class GameViewController: UIViewController {
    //MARK: Outlet's
    
    @IBOutlet var buttonView: UIView!
    @IBOutlet var GroundView: UIView!
    @IBOutlet var LeftRockView: UIView!
    @IBOutlet var RightRockView: UIView!
    @IBOutlet var AirForceView: UIView!
    @IBOutlet var actionView: UIView!
    @IBOutlet var rightButtonOutlet: UIButton!
    @IBOutlet var leftButtonOutlet: UIButton!
    @IBOutlet var scoreView: UITextField!
    @IBOutlet var liveView: UITextField!
    
    // MARK:  VAR's
    
    var gameScreen = UIImageView()
    var date = ""
    var playerName = "Player"
    var playerKills = 0
    var playerBonusPicked = 0
    var playerScores = 0
    var airForceName = ""
    var currentIndex = 0
    var currentIndexSecond = 0
    var scorePoint = 0 {
        didSet {
            self.scoreView.text = "Score: \(scorePoint)"
        }
    }
    var livePoint = 3 {
        didSet {
            self.liveView.text = "Live: \(livePoint)"
        }
    }
    var stopTimer = false
    var scoresArray: [Save] = []
    var motionManager = CMMotionManager()
    var xAccelerate: CGFloat = 0 {
        didSet {
            if xAccelerate > 0 {
                if self.airForce.frame.maxX >= self.RightRockView.frame.origin.x {
                    self.addingGameOverView()
                } else {
                    self.airForce.frame.origin.x += xAccelerate * step
                }
            } else if xAccelerate < 0 {
                if self.airForce.frame.minX <= self.LeftRockView.frame.maxX {
                    self.addingGameOverView()
                } else {
                    self.airForce.frame.origin.x -= xAccelerate * -step
                }
            }
        }
    }

    
    // MARK: LET's
    
    let leftRock = UIImageView()
    let rightRock = UIImageView()
    let airForce = UIImageView()
    let gameOver = UIImageView()
    let interval = 10.0
    let step : CGFloat = 20.0
    let enemyForceTwo = UIImageView()
    let birdImageArray = ["backgroundBirdTwoFirst", "backgroundBirdTwoSecond"]
    let birdImageArraySecond = ["backgroundBirdRightFirst", "backgroundBirdRightSecond"]

    
    //MARK: VC Life Func
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let airPict = UserDefaults.standard.value(forKey: "airPict") as? String else {return}
        self.airForceName = airPict
        guard let array = UserDefaults.standard.value([Save].self, forKey: "scores") else {return}
        self.scoresArray = array
        if motionManager.isAccelerometerAvailable {
                    motionManager.accelerometerUpdateInterval = 0.01
                    motionManager.startAccelerometerUpdates(to: .main) { [weak self] (data: CMAccelerometerData?, error: Error?) in
                        if let acceleration = data?.acceleration {
                            self?.xAccelerate = CGFloat(acceleration.x) * 0.75
                            print("x = " + "\(acceleration.x)")
                            print("y = " + "\(acceleration.y)")
                            print("z = " +  "\(acceleration.z)")
                            print()
                        }
                    }
                }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startGame()
    }
    
    //MARK: IBAction's
    
//    @IBAction func pressedButtonLeft(_ sender: UIButton) {
//        moveAirForceLeft()
//    }
//    @IBAction func pressedButtonRight(_ sender: UIButton) {
//        moveAirForceRight()
//    }
    @IBAction func gameOver(_ sender: UITapGestureRecognizer) {
        self.createPlayerScore()
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Flow func
    
    func startGame() {
        createGround()
        createRock()
        createAirForce(airForceName)
        startEnemyForce()
        startBackgroundCloud()
        startBackgroundBirdLeft()
        startBackgroundBirdRight()
        startBonusPack()
        self.view.insertSubview(scoreView, aboveSubview: GroundView)
        self.view.insertSubview(liveView, aboveSubview: GroundView)
//        self.view.insertSubview(rightButtonOutlet, aboveSubview: gameScreen)
//        self.view.insertSubview(leftButtonOutlet, aboveSubview: gameScreen)
    }
    func loadName() {
        guard let secondName = UserDefaults.standard.value(forKey: "Player name") as? String else {return}
        self.playerName = secondName
    }
    func createGround() {
        self.gameScreen.frame = CGRect(x: self.GroundView.frame.origin.x,
                                       y: self.GroundView.frame.origin.y,
                                       width: self.GroundView.frame.size.width,
                                       height: self.GroundView.frame.size.height)
        self.gameScreen.image = UIImage(named: "backgroundView")
        self.gameScreen.contentMode = .scaleToFill
        self.GroundView.addSubview(gameScreen)
    }
    func shiftIndex () {
        if currentIndex < birdImageArray.count - birdImageArray.count {
            currentIndex = birdImageArray.count - 1
        } else if currentIndex > birdImageArray.count - 1 {
            currentIndex = birdImageArray.count - birdImageArray.count
        }
    }
    func shiftIndexSecond () {
        if currentIndexSecond < birdImageArraySecond.count - birdImageArraySecond.count {
            currentIndexSecond = birdImageArraySecond.count - 1
        } else if currentIndexSecond > birdImageArraySecond.count - 1 {
            currentIndexSecond = birdImageArraySecond.count - birdImageArraySecond.count
        }
    }

    func moveAirForceLeft() {
        if self.airForce.frame.minX <= self.LeftRockView.frame.maxX {
            self.addingGameOverView()
        } else {
            self.airForce.frame.origin.x -= step
            self.airForce.frame.origin.x -= xAccelerate * 50
        }
    }
    func moveAirForceRight() {
        if self.airForce.frame.maxX >= self.RightRockView.frame.origin.x {
            self.addingGameOverView()
        } else {
            self.airForce.frame.origin.x += step
            self.airForce.frame.origin.x += xAccelerate * 50
        }
    }
    func startEnemyForce() {
        let timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { (timer) in
            if self.stopTimer == true {
                timer.invalidate()
            } else {
                self.addingEnemyForce()
            }
        }
    }
    func startBackgroundCloud() {
        let timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            if self.stopTimer == true {
                timer.invalidate()
            } else {
                self.addingBackgroundCloud()
            }
        }
    }
    func createAirForce(_ name: String) {
        let airForceWidth: Double = 50
        self.airForce.frame = CGRect(x: (Double(self.AirForceView.frame.size.width) / 2) - (Double(self.airForce.frame.size.width) / 2), y: Double(self.AirForceView.frame.origin.y), width: airForceWidth, height: airForceWidth)
        self.airForce.image = UIImage(named: name)
        self.airForce.contentMode = .scaleToFill
        self.GroundView.addSubview(airForce)
        
    }
    func addingEnemyForce() {
        let enemyForce = UIImageView()
        let enemyWidth: Double = 50
        let enemyHeight = enemyWidth
        let randomEnemyX: Double = .random(in: Double((self.actionView.frame.origin.x))...(Double(self.actionView.frame.width) - enemyWidth))
        enemyForce.frame = CGRect(x: randomEnemyX, y: Double(self.actionView.frame.origin.y) - enemyHeight, width: enemyWidth, height: enemyHeight)
        enemyForce.image = UIImage(named: "enemyAir")
        enemyForce.contentMode = .scaleAspectFill
        GroundView.addSubview(enemyForce)
        let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            self.checkedFrame(enemyForce.layer.presentation()?.frame, airForce: self.airForce.layer.presentation()?.frame, pict: enemyForce)
        }
        let timerAmmo = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { (timer) in
            self.enemyAmmo(enemyForce)
        }
        timerAmmo.fire()
        timer.fire()
        UIView.animate(withDuration: 6, delay: 0.0001, options: .curveLinear) {
            enemyForce.frame.origin.y += self.GroundView.frame.height + enemyForce.frame.height
        } completion: { (_) in
            self.scorePoint += 1
            self.playerKills += 1
            enemyForce.removeFromSuperview()
            
        }
    }
    func enemyAmmo(_ picture: UIImageView) {
        let enemyAmmo = UIImageView()
        let enemyAmmoWidht: Double = 25
        let enemyAmmoHeight = enemyAmmoWidht
        let enemyAmmoX = Double(picture.frame.midX)
        let enemyAmmoY = Double(picture.frame.origin.y + (picture.frame.height + CGFloat(enemyAmmoHeight)))
        enemyAmmo.frame = CGRect(x: enemyAmmoX, y: enemyAmmoY, width: enemyAmmoWidht, height: enemyAmmoHeight)
        enemyAmmo.image = UIImage(named: "enemyAmmo")
        enemyAmmo.contentMode = .scaleAspectFill
        enemyAmmo.alpha = 1.0
        GroundView.insertSubview(enemyAmmo, aboveSubview: gameScreen)
        let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            self.checkedFrame(enemyAmmo.layer.presentation()?.frame, airForce: self.airForce.layer.presentation()?.frame, pict: enemyAmmo)
        }
        UIView.animate(withDuration: 4, delay: 0, options: .curveLinear) {
            enemyAmmo.frame.origin.y += self.GroundView.frame.height + enemyAmmo.frame.height
        } completion: { (_) in
            enemyAmmo.removeFromSuperview()
        }
    }
    
    func addingBackgroundCloud() {
        let cloud = UIImageView()
        let cloudWidth: Double = 50
        let cloudHeight = cloudWidth
        let randomcloudX: Double = .random(in: Double((self.actionView.frame.origin.x))...(Double(self.actionView.frame.width) - cloudWidth))
        cloud.frame = CGRect(x: randomcloudX, y: Double(self.actionView.frame.origin.y) - cloudHeight, width: cloudWidth, height: cloudHeight)
        cloud.image = UIImage(named: "backgroundCloud")
        cloud.contentMode = .scaleAspectFill
        GroundView.insertSubview(cloud, belowSubview: airForce)
        UIView.animate(withDuration: 6, delay: 0.0001, options: .curveLinear) {
            cloud.frame.origin.y += self.GroundView.frame.height + cloud.frame.height
        } completion: { (_) in
            cloud.removeFromSuperview()
        }
    }
    func addingBonusPack() {
        let bonus = UIImageView()
        let bonusWidth: Double = 40
        let bonusHeight = bonusWidth
        let bonusX: Double = .random(in: Double((self.actionView.frame.origin.x))...(Double(self.actionView.frame.width) - bonusWidth))
        bonus.frame = CGRect(x: Double(CGFloat(bonusX)), y: Double(self.actionView.frame.minY) - bonusHeight, width: bonusWidth, height: bonusHeight)
        bonus.image = UIImage(named: "bonusPack")
        bonus.contentMode = .scaleAspectFill
        GroundView.addSubview(bonus)
        let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            self.checkedFrameAmmoPack(bonus.layer.presentation()?.frame, airForce: self.airForce.layer.presentation()?.frame, pack: bonus)
        }
        UIView.animate(withDuration: 6, delay: 0.0001, options: .curveLinear) {
            bonus.frame.origin.y += self.GroundView.frame.height + bonus.frame.height
        } completion: { (_) in
            bonus.removeFromSuperview()
        }
    }
    func startBonusPack() {
        let timer = Timer.scheduledTimer(withTimeInterval: 20, repeats: true) { [self] (timer) in
            if self.stopTimer == true {
                timer.invalidate()
            } else {
                addingBonusPack()
            }
        }
    }
    func startBackgroundBirdLeft() {
        let timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { [self] (timer) in
            if self.stopTimer == true {
                timer.invalidate()
            } else {
                addingBackgroundBirdLeft()
            }
        }
    }
    func addingBackgroundBirdLeft() {
        let bird = UIImageView()
        let birdWidth: Double = 70
        let birdHeight = birdWidth
        let randomBirdY: Double = .random(in: 0...(Double(self.GroundView.frame.height / 2)))
        bird.frame = CGRect(x: Double(self.GroundView.frame.origin.x - CGFloat(birdWidth)), y: randomBirdY, width: birdWidth, height: birdHeight)
        bird.image = UIImage(named: "\(self.birdImageArray[currentIndex])")
        bird.contentMode = .scaleAspectFill
        GroundView.insertSubview(bird, belowSubview: leftRock)
        UIView.animate(withDuration: 10, delay: 0.0001, options: .curveLinear) {
            bird.frame.origin.y += self.GroundView.frame.height + bird.frame.height
            bird.frame.origin.x += self.GroundView.frame.width + bird.frame.width
        } completion: { (_) in
            bird.removeFromSuperview()
        }
        
        func changeBird() {
            self.currentIndex += 1
            self.shiftIndex()
            bird.image = UIImage(named: "\(self.birdImageArray[currentIndex])")
        }
        let timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { [self] (timer) in
            changeBird()
        }
    }
    
    func addingBackgroundBirdRight() {
        let bird = UIImageView()
        let birdWidth: Double = 70
        let birdHeight = birdWidth
        let randomBirdY: Double = .random(in: 0...(Double(self.GroundView.frame.height / 2)))
        bird.frame = CGRect(x: Double(self.GroundView.frame.maxX), y: randomBirdY, width: birdWidth, height: birdHeight)
        bird.image = UIImage(named: "\(self.birdImageArraySecond[currentIndex])")
        bird.contentMode = .scaleAspectFill
        GroundView.insertSubview(bird, belowSubview: rightRock)
        UIView.animate(withDuration: 10, delay: 0.0001, options: .curveLinear) {
            bird.frame.origin.y += self.GroundView.frame.height + bird.frame.height
            bird.frame.origin.x -= self.GroundView.frame.width + bird.frame.width
        } completion: { (_) in
            bird.removeFromSuperview()
        }
        func changeBird() {
            self.currentIndexSecond += 1
            self.shiftIndexSecond()
            bird.image = UIImage(named: "\(self.birdImageArraySecond[currentIndex])")
        }
        let timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { [self] (timer) in
            changeBird()
        }
    }
    
    func startBackgroundBirdRight() {
        let timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { [self] (timer) in
            if self.stopTimer == true {
                timer.invalidate()
            } else {
                addingBackgroundBirdRight()
            }
        }
    }
    
    func checkedFrame(_ enemyForce: CGRect?, airForce:CGRect?, pict: UIImageView) {
        guard let enemyForce = enemyForce else {return}
        guard let force = airForce else {return}
        if enemyForce.intersects(force) {
            if self.livePoint == 0 {
                self.finishGame()
            } else {
                pict.removeFromSuperview()
                livePoint -= 1
            }
        }
    }
    
    func checkedFrameSideBackground(_ enemyForce: CGRect?, airForce:CGRect?) {
        guard let enemyForce = enemyForce else {return}
        guard let force = airForce else {return}
        if enemyForce.intersects(force) {
            if self.livePoint == 0 {
                self.finishGame()
            } else {
                livePoint -= 1
            }
        }
    }
    
    func finishGame() {
        self.stopTimer = true
        self.nukeAllAnimations()
        self.addingGameOverView()
        self.createDate()
        
    }
    
    func checkedFrameAmmoPack(_ enemyForce: CGRect?, airForce:CGRect?, pack:UIImageView) {
        guard let enemyForce = enemyForce else {return}
        guard let force = airForce else {return}
        if enemyForce.intersects(force) {
            pack.removeFromSuperview()
            self.scorePoint += 10
            self.livePoint += 1
            self.playerBonusPicked += 1
        }
    }
    
    func nukeAllAnimations() {
        self.GroundView.subviews.forEach({$0.layer.removeAllAnimations()})
        self.GroundView.layer.removeAllAnimations()
        self.GroundView.layoutIfNeeded()
        
    }
    func createPlayerScore() {
        let scores = Save.init(playerName: self.playerName, enemyKilled: self.playerKills, bonusPicked: self.playerBonusPicked, scorePoint: self.scorePoint, date: self.date)
        scoresArray.append(scores)
        let array = scoresArray
        UserDefaults.standard.set(encodable: array, forKey: "scores")
       
        
    }
    
    func createDate() {
        let today = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm dd.MM.yy"
        let string = formatter.string(from: today)
        self.date = string
    }
    
    func addingGameOverView() {
        self.gameOver.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        self.gameOver.image = UIImage(named: "gameOver")
        self.gameOver.contentMode = .scaleToFill
        self.view.addSubview(gameOver)
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(gameOver(_:)))
        self.view.addGestureRecognizer(recognizer)
    }
    
    func createRock() {
        self.leftRock.frame = CGRect(x: self.LeftRockView.frame.origin.x, y: self.LeftRockView.frame.origin.y, width: self.LeftRockView.frame.size.width, height: self.LeftRockView.frame.size.height)
        self.leftRock.image = UIImage(named: "sideViewLeft")
        self.leftRock.contentMode = .scaleToFill
        self.GroundView.addSubview(leftRock)
        
        self.rightRock.frame = CGRect(x: self.RightRockView.frame.origin.x, y: self.RightRockView.frame.origin.y, width: self.RightRockView.frame.size.width, height: self.RightRockView.frame.size.height)
        self.rightRock.image = UIImage(named: "sideViewRight")
        self.rightRock.contentMode = .scaleToFill
        self.GroundView.addSubview(rightRock)
    }
    
    func checkAirForceMove() {
        if self.airForce.frame.origin.x <= self.GroundView.frame.origin.x || self.airForce.frame.maxX >= self.GroundView.frame.maxX {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}



