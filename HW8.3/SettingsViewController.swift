import UIKit

enum SwipePictures {
    case back
    case forward
}
enum SwipeEnemyPictures {
    case back
    case forward
}
class SettingsViewController: UIViewController {
    
    
    @IBOutlet var settingsOutlet: UILabel!
    @IBOutlet var enemySkinsOutlet: UILabel!
    @IBOutlet var playerSkinsOutlet: UILabel!
    @IBOutlet var nameButtonOutlet: UIButton!
    @IBOutlet var nameOutlet: UILabel!
    @IBOutlet var backButtonOutlet: UIButton!
    @IBOutlet var textField: UITextField!
    @IBOutlet var enemyPictView: UIImageView!
    @IBOutlet var mainPictView: UIImageView!
    
    
    var imageArrayOne = ["airForce", "airForce2", "airForce3"]
    var enemyArray = ["enemyAir", "enemyAir2", "enemyAir3", "enemyAir4"]
    var enemyAirIndex = 0
    var currentIndex = 0
    var playerName = "Player"
    var date = ""
    var playerKills = 0
    var playerBonusPicked = 0
    var playerScores = 0
    var scorePoint = 0
    var playerScore = Save.init(playerName: "", enemyKilled: 0, bonusPicked: 0, scorePoint: 0, date: "")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let scores = UserDefaults.standard.value(Save.self, forKey: "Player scores") else {return}
        self.playerScore = scores
        addingImage()
        addingEnemyImage()
        setupLabel()
    }
    
    @IBAction func rightButton(_ sender: UIButton) {
        swipePicture(.forward, pict: mainPictView, string: imageArrayOne[currentIndex])
    }
    
    @IBAction func leftButton(_ sender: UIButton) {
        swipePicture(.back, pict: mainPictView, string: imageArrayOne[currentIndex])
    }
    
    @IBAction func pressButtonBack(_ sender: UIButton) {
        saveSettings()
        saveName()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func leftSwitchEnemy(_ sender: UIButton) {
        swipeEnemyPictures(.back, pict: enemyPictView, string: enemyArray[currentIndex])
    }
    
    @IBAction func rightSwitchEnemy(_ sender: UIButton) {
        swipeEnemyPictures(.forward, pict: enemyPictView, string: enemyArray[currentIndex])
    }
    
    func shiftIndex () {
        if currentIndex < imageArrayOne.count - imageArrayOne.count {
            currentIndex = imageArrayOne.count - 1
        } else if currentIndex > imageArrayOne.count - 1 {
            currentIndex = imageArrayOne.count - imageArrayOne.count
        }
    }
    
    func shiftIndexEnemy () {
        if enemyAirIndex < enemyArray.count - enemyArray.count {
            enemyAirIndex = enemyArray.count - 1
        } else if enemyAirIndex > enemyArray.count - 1 {
            enemyAirIndex = enemyArray.count - enemyArray.count
        }
    }
    
    func addingImage() {
        guard let airPict = UserDefaults.standard.value(forKey: "airPict") as? String else {return}
        
        mainPictView.image = UIImage(named: "\(airPict)")
        mainPictView.contentMode = .scaleAspectFit
        
    }
    func addingEnemyImage() {
        enemyPictView.image = UIImage(named: "\(enemyArray[currentIndex])")
        enemyPictView.contentMode = .scaleAspectFit
    }
    func swipePicture(_ swipe: SwipePictures, pict: UIImageView, string: String) {
        switch swipe {
        case .forward:
            if pict.image == nil {
                pict.image = UIImage(named: "\(string)")
                pict.contentMode = .scaleAspectFit
            } else {
                currentIndex += 1
                shiftIndex()
                pict.image = UIImage(named: "\(string)")
                pict.contentMode = .scaleAspectFit
            }
        case .back:
            currentIndex -= 1
            shiftIndex()
            pict.image = UIImage(named: "\(string)")
            pict.contentMode = .scaleAspectFit
        }
    }
    
    func swipeEnemyPictures(_ swipe: SwipeEnemyPictures, pict: UIImageView, string: String) {
        switch swipe {
        case .forward:
            enemyAirIndex += 1
            shiftIndexEnemy()
            pict.image = UIImage(named: "\(string)")
            pict.contentMode = .scaleAspectFit
            
        case .back:
            enemyAirIndex -= 1
            shiftIndexEnemy()
            pict.image = UIImage(named: "\(string)")
            pict.contentMode = .scaleAspectFit
        }
    }
    
    func setupLabel() {
        self.backButtonOutlet.dropShadow(15)
        self.backButtonOutlet.titleLabel?.font = UIFont(name: "Crosterian", size: 15)
        //        self.saveButtonOutlet.dropShadow(15)
        //        self.saveButtonOutlet.titleLabel?.font = UIFont(name: "Crosterian", size: 10)
        self.settingsOutlet.dropShadow(15)
        self.settingsOutlet.font = UIFont(name: "Crosterian", size: 20)
        self.enemySkinsOutlet.dropShadow(15)
        self.enemySkinsOutlet.font = UIFont(name: "Crosterian", size: 15)
        self.playerSkinsOutlet.dropShadow(15)
        self.playerSkinsOutlet.font = UIFont(name: "Crosterian", size: 15)
        self.nameOutlet.dropShadow(15)
        self.nameOutlet.font = UIFont(name: "Crosterian", size: 15)
        //        self.nameButtonOutlet.dropShadow(15)
        //        self.nameButtonOutlet.titleLabel?.font = UIFont(name: "Crosterian", size: 25)
        self.textField.text = playerScore.playerName
    }
    
    func saveName() {
        guard textField.text?.isEmpty == false else {return}
        self.playerName = textField.text!
        let name = self.playerName
        UserDefaults.standard.set(name, forKey: "player name")
    }
    
    func saveSettings() {
        let mainAir = imageArrayOne[currentIndex]
        let enemyAir = enemyArray[currentIndex]
        UserDefaults.standard.set(mainAir, forKey: "airPict")
        UserDefaults.standard.set(enemyAir, forKey: "enemyPict")
        let scores = Save.init(playerName: self.playerName, enemyKilled: self.playerKills, bonusPicked: self.playerBonusPicked, scorePoint: self.scorePoint, date: self.date)
        UserDefaults.standard.set(encodable: scores, forKey: "Player scores")
    }
}
