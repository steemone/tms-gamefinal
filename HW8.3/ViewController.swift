import UIKit

class ViewController: UIViewController {
    @IBOutlet var mainPicture: UIImageView!
    @IBOutlet var settingsButtonOutlet: UIButton!
    @IBOutlet var scoreButtonOutlet: UIButton!
    @IBOutlet var startButtonOutlet: UIButton!
    
    let sizeFontButton: CGFloat = 15.0
    
    override func viewDidAppear(_ animated: Bool) {
        addingMainPicture()
        setupButtons()
    }
    
//    @IBAction func crashButtonTapped(_ sender: AnyObject) {
//        fatalError()
//    }
    
    @IBAction func pressStartGame(_ sender: UIButton) {
        guard let startGame = self.storyboard?.instantiateViewController(withIdentifier: "GameViewController") as? GameViewController else { return }
        self.navigationController?.pushViewController(startGame, animated: true)
    }
    
    @IBAction func pressButtonScore(_ sender: UIButton) {
        guard let scoreGame = self.storyboard?.instantiateViewController(withIdentifier: "ScoreViewController") as? ScoreViewController else { return }
        self.navigationController?.pushViewController(scoreGame, animated: true)
    }
    
    @IBAction func pressButtonSettings(_ sender: UIButton) {
        guard let settings = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else { return }
        self.navigationController?.pushViewController(settings, animated: true)
    }
    
    
//    func createErrorButton() {
//        let button = UIButton(type: .roundedRect)
//        button.frame = CGRect(x: 20, y: 50, width: 100, height: 30)
//        button.setTitle("Crash", for: [])
//        button.addTarget(self, action: #selector(self.crashButtonTapped(_:)), for: .touchUpInside)
//        view.addSubview(button)
//    }
    
    func addingMainPicture() {
        self.mainPicture.frame = CGRect(x: self.mainPicture.frame.origin.x, y: self.mainPicture.frame.origin.y, width: self.mainPicture.frame.size.width, height: self.mainPicture.frame.size.height)
        self.mainPicture.image = UIImage(named: "customlogo")
        self.mainPicture.contentMode = .scaleToFill
        self.view.addSubview(mainPicture)
    }
    
    func setupButtons() {
        self.view.backgroundColor = .black
        self.startButtonOutlet.dropShadow(15)
        self.startButtonOutlet.backgroundColor = .blue
        self.settingsButtonOutlet.dropShadow(15)
        self.settingsButtonOutlet.backgroundColor = .blue
        self.scoreButtonOutlet.dropShadow(15)
        self.scoreButtonOutlet.backgroundColor = .blue
        self.startButtonOutlet.titleLabel?.font = UIFont(name: "Crosterian", size: sizeFontButton)
        self.scoreButtonOutlet.titleLabel?.font = UIFont(name: "Crosterian", size: sizeFontButton)
        self.settingsButtonOutlet.titleLabel?.font = UIFont(name: "Crosterian", size: sizeFontButton)
    }
    
}


