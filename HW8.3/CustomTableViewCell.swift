import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerKilled: UILabel!
    @IBOutlet weak var playerBonus: UILabel!
    @IBOutlet weak var playerScores: UILabel!
    @IBOutlet weak var dateGame: UILabel!
    
    var labelSize: CGFloat = 12
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLabel()
    }
    
    func configure(object: Save) {
        if let name = object.playerName {
            self.playerName.text = ("Name:".localized + "\(name)")
        }
        if let killed = object.enemyKilled {
            self.playerKilled.text = ("Kill:".localized +     "\(killed)")
        }
        if let bonus = object.bonusPicked {
            self.playerBonus.text = ("Bonus:".localized +   "\(bonus)")
        }
        if let score = object.scorePoint {
            self.playerScores.text = ("Scores:".localized + "\(score)")
        }
        if let date = object.date {
            self.dateGame.text = ("Date:".localized +  "\(date)")
        }
    }
    func setupLabel() {
        self.playerName.dropShadow(15)
        self.playerName.font = UIFont(name: "Crosterian", size: labelSize)
        self.playerKilled.dropShadow(15)
        self.playerKilled.font = UIFont(name: "Crosterian", size: labelSize)
        self.playerScores.dropShadow(15)
        self.playerScores.font = UIFont(name: "Crosterian", size: labelSize)
        self.playerBonus.dropShadow(15)
        self.playerBonus.font = UIFont(name: "Crosterian", size: labelSize)
        self.dateGame.dropShadow(15)
        self.dateGame.font = UIFont(name: "Crosterian", size: 10)
    }

}
