
import Foundation

class Save: Codable {
    var playerName: String?
    var enemyKilled: Int? = 0
    var bonusPicked: Int? = 0
    var scorePoint: Int? = 0
    var date: String?
    
    init(playerName: String?, enemyKilled: Int?, bonusPicked: Int?, scorePoint: Int?, date: String?){
        self.playerName = playerName
        self.enemyKilled = enemyKilled
        self.bonusPicked = bonusPicked
        self.scorePoint = scorePoint
        self.date = date
    }
    private enum CodingKeys: String, CodingKey {
        case playerName
        case enemyKilled
        case bonusPicked
        case scorePoint
        case date
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        playerName = try container.decodeIfPresent(String.self, forKey: .playerName)
        enemyKilled = try container.decodeIfPresent(Int.self, forKey: .enemyKilled)
        bonusPicked = try container.decodeIfPresent(Int.self, forKey: .bonusPicked)
        scorePoint = try container.decodeIfPresent(Int.self, forKey: .scorePoint)
        date = try container.decodeIfPresent(String.self, forKey: .date)
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.playerName, forKey: .playerName)
        try container.encode(self.enemyKilled, forKey: .enemyKilled)
        try container.encode(self.bonusPicked, forKey: .bonusPicked)
        try container.encode(self.scorePoint, forKey: .scorePoint)
        try container.encode(self.date, forKey: .date)
    }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
